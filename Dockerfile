FROM golang:1.8-alpine

RUN mkdir /app 

COPY . /app/ 

WORKDIR /app 

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .

# methode de l'elite
#nRUN addgroup -S appgroup && adduser -S appuser -G appgroup

# methode tp2
RUN adduser -D my-user -u 1000
USER 1000

HEALTHCHECK NONE

CMD ["/app/main"]

EXPOSE 80
